# Author: Yagil Itzhak

# Program goal:
#     Create a hangman man game

# Program process:
#     <1> Print hangman man assci art in the console
#     <2> Create a new <integer> variable called user_lifes_left (responsible..
#          for user lifes left number)
#     <3> Ask the user to type the [path for the file that the "secret word"..
#          in it] and the [index of the word in the file] in the console
#     <4> Create a new <string> variable called..
#          user_request_for_a_the_secret_word (responsible for the secret word)
#     <5> Request from the user a console input and put it's value in the..
#          variable user_request_for_a_the_secret_word
#     <6> Choose a word acording to the value of..
#          user_request_for_a_the_secret_word and put it in the variable..
#           user_request_for_a_the_secret_word
#     <7> Convert the value of user_request_for_a_the_secret_word to lower case
#     <8> Create a new <set> variable called letter_guessed
#     <9> Print "_ " the lenghth of the value of the variable..
#          user_request_for_a_the_secret_word times
#     <10> Create a new <string> variable called user_request_for_a_letter..
#           (responsible for the user last try for guessing a letter)
#     <11> Until user_lifes_left is not equal to zero do:
#     <12>      Print to the user the known part of the secret word
#     <13>      Ask the user to give console request for guess of letter
#     <14>      Request form the user a console input and put it's value in..
#                the variable user_request_for_a_letter
#     <15>      Convert the value of user_request_for_a_letter to lower case
#     <16>      If the request of the user is unvalid -> Print 'X' in the..
#                console and jamp to line 13
#     <17>      If user_request_for_a_letter exist in..
#                user_request_for_a_the_secret_word do:
#     <18>              Add user_request_for_a_letter to the set letter_guessed
#     <19>              If the user is win -> Print win in the console and..
#                        then exit from the program
#     <20>      Otherwise do:
#     <21>      Decrease the value of user_lifes_left by one
#     <22>      If the user is loss -> Print loss in the console  and then..
#                exit from the program

def main():
    start_game()
    path_file = input("Enter file path: ")
    index = int(input("Enter index: "))
    secret_word = choose_word(path_file, index)
    MAX_TRIES = 6
    num_of_tries = 1
    old_letters_guessed = list()
    print_hangman(num_of_tries)
    while num_of_tries <= MAX_TRIES:
        if check_win(secret_word, old_letters_guessed):
            break
        while True:
            print(show_hidden_word(secret_word, old_letters_guessed))
            letter_guessed = input("Guess a letter: ").lower()
            if try_update_letter_guessed(letter_guessed,old_letters_guessed):
                break
        if letter_guessed not in secret_word:
            num_of_tries += 1
            print("X")
            print_hangman(num_of_tries)
        old_letters_guessed.append(letter_guessed)
    if check_win(secret_word, old_letters_guessed):
        print(show_hidden_word(secret_word, old_letters_guessed))
        print("""           _
              (_)
     __      ___ ____
     \\ \\ /\\ / / |  _ \\
      \\      /| | | | |
       \\_/\\_/ |_|_| |_|""")
    else:
        print(show_hidden_word(secret_word, old_letters_guessed))
        print("""  _
     | |
     | | ___  ___ ___
     | |/ _ \\/ __/ __|
     | | (_) \\__ \\__ \\
     |_|\\___/|___/___/""")


def start_game():
    '''
    this function printing the opening screen
    :return: none
    '''
    HANGMAN_ASCII_ART ="""
      _    _
     | |  | |
     | |_| | _ _ _ _   _ _ _ _ __   _ _ _ _
     |  _  |/ _` | ' \ / ` | ' ` _ \ / ` | ' \
     | |  | | (| | | | | (| | | | | | | (_| | | | |
     ||  ||\_,|| ||\_, || || ||\_,|| ||
                          __/ |
                         |_/"""
    MAX_TRIES = 6

    print(HANGMAN_ASCII_ART)
    print(MAX_TRIES)

def choose_word(file_path, index):
    """
    This function returns a random word from a given file
    :input: the path of the file, the index of the word in the file
    :output: the chosen word
    """
    words_file = open(file_path, 'r')
    words_str = words_file.read()
    list_word = words_str.split(" ")
    index = index % len(list_word)-1
    return list_word[index]

def print_hangman(num_of_tries):
    '''
    this function print the hangman according to the player's wrong guessing tries
    :param num_of_tries: the number of the wrong tries
    :type num_of_tries: int
    :return: none
    '''
    HANGMAN_PHOTOS = {
    1:"""
        x-------x""""",
    2:"""
        x-------x
        |
        |
        |
        |
        |""",
    3:"""
        x-------x
        |       |
        |       0
        |
        |
        |""",
    4:"""
        x-------x
        |       |
        |       0
        |       |
        |
        |""",
    5:"""
        x-------x
        |       |
        |       0
        |      /|\\
        |
        |""",
    6:"""
        x-------x
        |       |
        |       0
        |      /|\\
        |      /
        |""",
    7:"""
        x-------x
        |       |
        |       0
        |      /|\\
        |      / \\
        |"""}
    HANGMAN_PHOTOS[num_of_tries]

    print(HANGMAN_PHOTOS[num_of_tries])

def check_win(secret_word, old_letters_guessed):
    '''
    This function returns is the user win
    :input: the secret word, the letters that the user guess until now
    :output: is the user win
    '''
    new_word_list = []
    for i in range(len(secret_word)):
        if secret_word[i] in old_letters_guessed:
            new_word_list.append(secret_word[i])
        else:
            new_word_list.append('_')
    new_word = "".join(new_word_list)
    return new_word == secret_word

def show_hidden_word(secret_word, old_letters_guessed):
    """
    This function will print to the user the known part of the secret word
    :input: the secret word, the letter that the user gusse until now
    :output: the known part to the user of the secret word
    """
    new_word_list = []
    for i in range(len(secret_word)):
        if secret_word[i] in old_letters_guessed:
            new_word_list.append(secret_word[i])
        else:
            new_word_list.append('_')
    new_word = " ".join(new_word_list)
    return new_word

def check_valid_input(letter_guessed, old_letters_guessed):
    """
    This function returns is the user request for a letter valid
    :input: the letter that the letter guessed, all of the letter that the..
     user gusse untill now
    :output: is the letter is valid
    """
    letter_guessed = letter_guessed.lower()
    if (letter_guessed.isalpha() == False) or (len(letter_guessed) > 1) or (letter_guessed in old_letters_guessed): #if the letter is longer than one char or have chars that are not a letter or guessed already
        return False
    else: #if it is correct
        return True

def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    This function will try to update guessed letter to the list..
     old_letters_guessed
    :input: the letter that the letter guessed, all of the letter that the..
     user gusse until now
    :output: is the letter is valid
    """
    letter_guessed = letter_guessed.lower()
    old_letters_guessed = sorted(old_letters_guessed)
    if not check_valid_input(letter_guessed, old_letters_guessed):
        print("X")
        print(" -> ".join(old_letters_guessed))
        return False
    else: #if it is correct
        old_letters_guessed.append(letter_guessed)
        return True

if __name__ == '__main__':
    main()
